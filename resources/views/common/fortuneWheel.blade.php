<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
        <title>{{ env('APP_NAME') }}</title>
        <link rel="stylesheet" href="{{ asset('css/fortune-wheel.css?'.uniqid()) }}">
    </head>
    <body>
        <div class="form-wrapper active">
            <div class="form-content">
                <h1 class="form-title">Колесо фортуны</h1>
                <form name="user-data">
                    <label class="form-label">
                        <span class="form-input-title">Ваше имя</span>
                        <input class="form-input" type="text" name="name" placeholder="Джон Смит" required>
                    </label>
                    <label class="form-label">
                        <span class="form-input-title">Ваше номер телефона</span>
                        <input class="form-input" type="text" name="phone" data-input-type="phone" placeholder="+7 (700) 999 55 88" required>
                    </label>
                    <label class="form-label">
                        <span class="form-input-title">Номер заказа</span>
                        <span class="form-input-description">
                            Номер заказа указан в Вашем чеке.
                            Чек должен быть на Вашей электронной почте.
                        </span>
                        <input class="form-input" type="text" name="order_id" placeholder="0123456789" required>
                    </label>
                    <button class="form-input-button" type="submit">Перейти к колесу</button>
                </form>
            </div>
        </div>
        <div class="fortune-wrapper">
            <div class="fortune-content">
                <div class="fortune-group__wrapper">
                    <ul class="fortune-group">
                        <li class="fortune-item">
{{--                            <img class="fortune-item__image" src="{{ asset('preview-image.png') }}" alt="product">--}}
                            <span class="fortune-item__title">Приз 1</span>
{{--                            <small>0%</small>--}}
                        </li>
                    </ul>
                </div>
                <div class="fortune-inner__wrapper">
                    <div class="fortune-inner">
                        <div class="fortune-inner__circle">
                            <span class="fortune-inner__title">qamalladin</span>
                            <span class="fortune-inner__title">education</span>
                        </div>
                        <div class="fortune-inner__arrow"></div>
                        <button class="fortune-inner__button" disabled>
                            Крутануть<br> и забрать приз
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="result-wrapper">
            <div class="result-content form-content">
                <h1 class="form-title">Поздравляем!</h1>
                <div class="result-product__info">
                    <div class="result-product__text">
                        <p id="result-description-first" class="result-text">
                            Вы выиграли <b>NAME</b>.
                        </p>
                        <p id="result-description-second" class="result-text">
                            <b>Описание:</b><br>
                            DESCRIPTION
                        </p>
                    </div>
                    <div class="result-product__image">
                        <img id="result-image" class="result-product__image-item" src="{{ asset('assets/logo.png') }}" alt="product">
                    </div>
                </div>
                <p id="result-description-last" class="result-text">
                    Ваш приз будет доставлен к Вам вместе с Вашим заказом. Желаем приятного дня и покупок :)
                </p>
                <button id="result-confirm-button" class="form-input-button">Понятно</button>
                <p id="result-timer-text" class="result-text">
{{--                    Данная страница будет автоматически закрыта через <b>20</b> секунд...--}}
                </p>
            </div>
        </div>
        <script src="{{ asset('js/inputmask.min.js') }}"></script>
        <script src="{{ asset('js/fortune-wheel.js?'.uniqid()) }}"></script>
    </body>
</html>
