<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
        <title>{{ env('APP_NAME') }}</title>
        <link rel="stylesheet" href="{{ asset('css/fortune-wheel.css?'.uniqid()) }}">
    </head>
    <body>
        <div id="fortune-wrapper">
            <div id="fortune-start" class="active">
                <p id="fortune-start-text">
                    Поздравляем! У Вас есть возможность прокрутить колесо фортуны и получить подарок!
                </p>
                <button id="fortune-start-button">Испытать удачу</button>
            </div>
            <div id="fortune-canvas">
                <div id="fortune-canvas-arrow"></div>
                <canvas id="fortune-canvas-item" width="1000" height="500"></canvas>
                <button id="fortune-canvas-button">
                    Крутануть<br> и забрать приз
                </button>
            </div>
        </div>
        <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ asset('js/gsap.min.js') }}"></script>
        <script src="{{ asset('js/Winwheel.min.js') }}"></script>
        <script>
            $('#fortune-start-button').click(function(event) {
                event.preventDefault();

                $('#fortune-start').removeClass('active');
                $('#fortune-canvas').addClass('active');

                const Wheel = new Winwheel({
                    canvasId: 'fortune-canvas-item',
                    fillStyle: '#fff',
                    strokeStyle: 'rgba(203,86,42,1)',
                    textFillStyle: '#000',
                    textAligment: 'center',
                    pointerAngle: 0,
                    innerRadius: 40,
                    lineWidth: 4,
                    numSegments: 7,
                    segments: [
                        { text: 'Приз 1', textFontWeight: 'bold' },
                        { text: 'Приз 2', textFontWeight: 'bold' },
                        { text: 'Приз 3', textFontWeight: 'bold' },
                        { text: 'Приз 4', textFontWeight: 'bold' },
                        { text: 'Приз 5', textFontWeight: 'bold' },
                        { text: 'Приз 6', textFontWeight: 'bold' },
                        { text: 'Приз 7', textFontWeight: 'bold' },
                    ],
                    animation: {
                        type: 'spinToStop',
                        duration: 5,
                        spins: 10,
                        callbackFinished() {
                            const segmentWin = Wheel.getIndicatedSegment();

                            console.log(segmentWin);
                            alert(`Ваш при: ${segmentWin.text}`);
                        }
                    },
                });

                $('#fortune-canvas-button').click(function() {
                    $(this).attr('disabled', true);
                    Wheel.startAnimation();
                });
            });
        </script>
    </body>
</html>
