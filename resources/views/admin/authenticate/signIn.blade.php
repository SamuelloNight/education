@extends('layouts.authenticate')

@section('content')
    <div class="wrapper vh-100">
        <div class="row align-items-center h-100">
            <form class="col-lg-3 col-md-4 col-10 mx-auto">
                <div class="mx-auto text-center">
                    <h3 class="mb-2">{{ env('APP_NAME') }}</h3>
                    <span class="d-block mb-4">Вход в панель администратора</span>
                </div>
                <div class="form-group">
                    <label for="inputEmail">Электронный адресс</label>
                    <input class="form-control" id="inputEmail" type="email" placeholder="example@gmail.com" required autofocus>
                </div>
                <div class="form-group mb-4">
                    <label for="inputPassword">Пароль</label>
                    <input class="form-control" id="inputPassword" type="password" placeholder="qwerty123456" required>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Выполнить вход</button>
                <p class="mt-5 mb-3 text-muted text-center">&copy; {{ env('APP_NAME') }} &bull; 2021 &bull; developed by Rustam Keldikov.</p>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('form').submit(function(event) {
            event.preventDefault();

            const formData = new FormData();
            const data = {
                email: $('#inputEmail').val(),
                password: $('#inputPassword').val(),
            };

            formData.append('email', data.email);
            formData.append('password', data.password);

            $.ajax({
                url: '{{ route('apiAdminAuthSignIn') }}',
                context: this,
                data: formData,

                success(response) {
                    if (response.status) {
                        window.location = '{{ route('adminDashboard') }}';
                    } else {
                        alert(response.message);
                    }
                },
            });
        });
    </script>
@endsection
