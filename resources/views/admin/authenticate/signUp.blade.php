@extends('layouts.authenticate')

@section('content')
    <div class="wrapper vh-100">
        <div class="row align-items-center h-100">
            <form class="col-lg-6 col-md-8 col-10 mx-auto">
                <div class="mx-auto text-center">
                    <h3 class="mb-2">{{ env('APP_NAME') }}</h3>
                    <span class="d-block">Здравствуйте <b>{{ $clientName }}</b>, Вам выдали доступ для администрирования.</span>
                    <span class="d-block mb-4">Пожалуйста, заполните данные для нового аккаунта. Они будут нужные для авторизации в дальнейшем.</span>
                </div>
                <div class="form-group">
                    <label for="inputEmail">Электронный адресс</label>
                    <input class="form-control" id="inputEmail" type="email" placeholder="example@gmail.com" required autofocus>
                </div>
                <div class="form-row mb-4">
                    <div class="form-group col-md-6">
                        <label for="inputPassword">Пароль</label>
                        <input class="form-control" id="inputPassword" type="password" placeholder="qwerty123456" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPasswordConfirm">Подтвердите пароль</label>
                        <input class="form-control" id="inputPasswordConfirm" type="password" placeholder="qwerty123456" required>
                    </div>
                </div>
                <button class="btn btn-lg btn-primary btn-block" id="submit" type="submit">Зарегистрироваться</button>
                <p class="mt-5 mb-3 text-muted text-center">&copy; {{ env('APP_NAME') }} &bull; 2021 &bull; developed by Rustam Keldikov.</p>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('form').submit(function(event) {
            event.preventDefault();

            const formData = new FormData();
            const data = {
                email: $('#inputEmail').val(),
                password: {
                    original: $('#inputPassword').val(),
                    confirm: $('#inputPasswordConfirm').val(),
                },
            };

            if (data.password.original !== data.password.confirm) {
                return alert('Пароли не совпадают, проверьте их валидность.');
            }

            formData.append('name', '{{ $clientName }}');
            formData.append('email', data.email);
            formData.append('password', data.password.original);
            formData.append('token', '{{ $token }}');

            $.ajax({
                url: '{{ route('apiAdminAuthSignUp') }}',
                context: this,
                data: formData,

                success(response) {
                    if (response.status) {
                        window.location = '{{ route('adminDashboard') }}';
                    } else {
                        alert(response.message);
                    }
                },
            });
        });
    </script>
@endsection
