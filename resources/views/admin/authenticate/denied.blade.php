@extends('layouts.authenticate')

@section('content')
    <div class="wrapper vh-100 d-flex justify-content-center align-items-center">
        @if($type === 'signUp')
            <p class="d-block mb-4">
                В регистрации аккаунта администратора - отказано.<br>
                Токен для регистрации недействителен.<br>
                Сообщите администрации для выдачи корректной или новой ссылки.
            </p>
        @else
            <p class="d-block mb-4">
                В доступе авторизации отказано.
            </p>
        @endif
    </div>
@endsection
