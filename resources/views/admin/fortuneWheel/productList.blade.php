@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12 mb-4">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">Таблица призов</strong>
                </div>
                <div class="card-body">
                    <table class="table table-borderless table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Превью</th>
                                <th>Название</th>
                                <th class="w-25">Описание</th>
                                <th>Шанс выподения</th>
                                <th>Имя администратора</th>
                                <th>Дата создания</th>
                                <th>Дата редактирования</th>
                                <th>Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                                <tr data-product-id="{{ $product->getProductId() }}">
                                    <td>{{ $product->getProductId() }}</td>
                                    <td>
                                        <div class="avatar avatar-md">
                                            <img src="{{ $product->getProductPreview() }}" alt="..." class="avatar-img rounded-circle">
                                        </div>
                                    </td>
                                    <td>{{ $product->getProductName() }}</td>
                                    <td class="w-25">{{ $product->getProductDescription() }}</td>
                                    <td>{{ $product->getProductChance() }}.00%</td>
                                    <td>{{ $product->getProductAdmin()->getClientName() }}</td>
                                    <td>{{ $product->getProductCreatedAt() }}</td>
                                    <td>{{ $product->getProductUpdatedAt() }}</td>
                                    <td>
                                        <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="{{ route('adminFortuneWheelEdit', ['id' => $product->getProductId()]) }}">Редактировать</a>
                                            <a class="dropdown-item" href="#" data-product-remove="{{ $product->getProductId() }}">Удалить</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('[data-product-remove]').click(function(event) {
            event.preventDefault();

            if (confirm('Вы точно хотите удалить данный приз из ротации?')) {
                const formData = new FormData();

                formData.append('product_id', $(this).attr('data-product-remove'));

                $.ajax({
                    url: '{{ route('apiAdminFortuneWheelDelete') }}',
                    data: formData,
                    context: this,

                    success(response) {
                        alert(response.message);

                        if (response.status === true) {
                            $(`[data-product-id="${$(this).attr('data-product-remove')}"]`).remove();
                        }
                    }
                });
            }
        });
    </script>
@endsection
