window.JQuery = window.$ = require('jquery')

/**
 * Количество секунд после которого будет
 * автоматижеское перенаправление на основной сайт
 * Если значение стоит меньше 5, то таймер будет отключен
 */
let automaticRedirectionDelay = 50

/**
 * Сам скрипт для работы с интерфейсом клиента
 */
$(() => {
    new Inputmask('+7 (999) 999 99 99').mask($('[data-input-type="phone"]'))

    const fortuneWheelInitialization = (name, phone, orderId) => {
        $.ajax({
            url: '/api/customer/fortune-wheel/scroll',
            data: {
                customer_name: name,
                customer_phone: phone,
                order_id: orderId
            },

            success(response) {
                const fortuneGroup = $('.fortune-group')

                fortuneGroup.each((groupIndex, groupItem) => {
                    $(groupItem).html('')

                    response.forEach((productItem) => {
                        $(groupItem).append(`
                            <li class="fortune-item">
                                <span class="fortune-item__title">${productItem.name}</span>
                            </li>
                        `)
                    })
                });

                $('.form-wrapper').removeClass('active');
                $('.fortune-wrapper').addClass('active');

                $('.fortune-inner__button').removeAttr('disabled').click((event) => {
                    event.preventDefault()

                    const wheelStartAudio = new Audio('/assets/sounds/fortune-wheel-start.mp3')
                    const wheelEndAudio = new Audio('/assets/sounds/fortune-wheel-end.mp3')
                    const wheelElement = $('.fortune-group')
                    let wheelTransformValue = 'rotateZ(1260deg)'

                    if ($(window).width() <= 425) {
                        wheelTransformValue = 'rotateZ(1260deg) scale(0.8)'
                    }

                    wheelStartAudio.play()

                    wheelElement.css({
                        transform: wheelTransformValue
                    });

                    setTimeout(() => {
                        wheelEndAudio.play()

                        $('#result-description-first').html(`
                            Вы выиграли <b>${response[4].name.replace('<br>', '')}</b>.
                        `)

                        $('#result-description-second').html(`
                            ${response[4].description}
                        `)

                        $('#result-image').attr('src', response[4].preview)

                        $('#result-confirm-button').click(() => {
                            window.location = 'http://qamalladin.education/'
                        })

                        if (automaticRedirectionDelay > 5) {
                            setTimeout(() => {
                                window.location = 'http://qamalladin.education/'
                            }, automaticRedirectionDelay * 1000)

                            setInterval(() => {
                                automaticRedirectionDelay--

                                $('#result-timer-text').html(`
                                    Данная страница будет автоматически закрыта через <b>${automaticRedirectionDelay}</b> секунд...
                                `)
                            }, 1000)
                        }

                        setTimeout(() => {
                            $('.form-wrapper').remove()
                            $('.fortune-wrapper').remove()
                            $('.result-wrapper').addClass('active')
                        }, 500)
                    }, 10000)
                })
            }
        });
    }

    $('form[name="user-data"]').submit((event) => {
        event.preventDefault()

        $(this).find('button[type="submit"]').text('Загрузка...').attr('disabled', true)

        const form = $('form[name="user-data"]')

        fortuneWheelInitialization(
            $(form).find('input[name="name"]').val(),
            $(form).find('input[name="phone"]').val(),
            $(form).find('input[name="order_id"]').val()
        )
    })
});

