<?php

use App\Http\Controllers\Api\Admin\AuthenticateController;
use App\Http\Controllers\Api\Admin\FortuneWheelController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->group(function() {
    Route::middleware('admin')->group(function() {
        Route::prefix('fortune-wheel')->group(function() {
            Route::post('create', [FortuneWheelController::class, 'productCreate'])->name('apiAdminFortuneWheelCreate');
            Route::post('update', [FortuneWheelController::class, 'productUpdate'])->name('apiAdminFortuneWheelUpdate');
            Route::post('delete', [FortuneWheelController::class, 'productDelete'])->name('apiAdminFortuneWheelDelete');
        });

        Route::prefix('winners')->group(function() {
            Route::prefix('customers')->group(function() {
                Route::post('update-status', [FortuneWheelController::class, 'productWinnerStatusUpdate'])->name('apiAdminFortuneWheelWinnerUpdateStatus');
            });
        });
    });

    Route::prefix('authenticate')->group(function() {
        Route::post('sign-in', [AuthenticateController::class, 'signIn'])->name('apiAdminAuthSignIn');
        Route::post('sign-up', [AuthenticateController::class, 'signUp'])->name('apiAdminAuthSignUp');
    });
});
