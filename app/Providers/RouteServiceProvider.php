<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    public const HOME = '/';

    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            $this->apiRoutes();
            $this->webRoutes();
        });
    }

    protected function apiRoutes()
    {
        $routes = glob(base_path('routes/api/*.php'));

        Route::prefix('api')->group(function() use($routes) {
            foreach ($routes as $route) {
                Route::middleware('web')->group($route);
            }
        });
    }

    protected function webRoutes()
    {
        $routes = glob(base_path('routes/web/*.php'));

        foreach ($routes as $route) {
            Route::middleware('web')->group($route);
        }
    }

    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
