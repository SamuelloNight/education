<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductWinner;
use Illuminate\Http\Request;

class FortuneWheelController extends Controller
{
    public function productCreate(Request $request): array
    {
        $productName = $request->post('name');
        $productDescription = $request->post('description');
        $productPreview = $request->file('preview');
        $productPreviewExtension = $productPreview->getClientOriginalExtension();
        $productPreviewName = md5(md5(uniqid()).md5(rand(1,32))).'.'.$productPreviewExtension;
        $productChance = $request->post('chance');

        if ($productChance > 100) {
            $productChance = 100;
        } else if ($productChance < 1) {
            $productChance = 1;
        }

        $productCreate = Product::create([
            'admin_id' => admin()->id(),
            'name' => $productName,
            'description' => str_replace('\n', '<br>', $productDescription),
            'preview' => "/storage/products/{$productPreviewName}",
            'chance' => $productChance,
        ]);

        if ($productCreate) {
            $productPreview->storeAs('public/products', $productPreviewName);

            return [
                'status' => true,
                'data' => $productCreate,
                'message' => "Приз успешно добавлен. Имеет \"{$productChance}\" процентов шанса выпадения.",
            ];
        }

        return [
            'status' => false,
            'data' => $request->all(),
            'message' => 'Системная ошибка. Проверьте правильно ли вы заполнили данные, если не поможет, сообщите администрации.',
        ];
    }

    public function productUpdate(Request $request): array
    {
        $productPreview = $request->file('preview');

        $request->merge([
            'description' => str_replace('\n', '<br>', $request->post('description')),
        ]);

        if ($productPreview) {
            $product = $request->all();
            $productPreviewExtension = $productPreview->getClientOriginalExtension();
            $productPreviewName = md5(md5(uniqid()).md5(rand(1,32))).'.'.$productPreviewExtension;
            $product['preview'] = "/storage/products/{$productPreviewName}";

            $productPreview->storeAs('public/products', $productPreviewName);

            $productUpdate = Product::find($request->post('product_id'))->update($product);
        } else {
            $productUpdate = Product::find($request->post('product_id'))->update($request->all());
        }

        if ($productUpdate) {
            return [
                'status' => true,
                'data' => null,
                'message' => 'Приз успешно обновлён. Можете продолжать :)'
            ];
        }

        return [
            'status' => false,
            'data' => null,
            'message' => 'Системная ошибка. Проверьте правильно ли вы заполнили данные, если не поможет, сообщите администрации.'
        ];
    }

    public function productDelete(Request $request): array
    {
        $productDelete = Product::find($request->post('product_id'))->delete();

        if ($productDelete) {
            return [
                'status' => true,
                'data' => null,
                'message' => 'Приз успешно удалён из ротации.'
            ];
        }

        return [
            'status' => false,
            'data' => null,
            'message' => 'Системная ошибка. Проверьте правильно ли вы заполнили данные, если не поможет, сообщите администрации.'
        ];
    }

    public function productWinnerStatusUpdate(Request $request): array
    {
        $productWinnerId = $request->post('product_id');
        $productWinnerStatus = $request->post('product_status');
        $productWinner = ProductWinner::find($productWinnerId);

        $productWinner->update([
            'status' => $productWinnerStatus
        ]);

        return [
            'status' => true,
            'message' => 'Статус приза успешно обновлён.'
        ];
    }
}
