<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductWinner;
use Illuminate\Http\Request;

class FortuneWheelController extends Controller
{
    public function scroll(Request $request)
    {
        $customerName = $request->get('customer_name');
        $customerPhone = $request->get('customer_phone');
        $orderId = $request->get('order_id');

        if ($customerName && $customerPhone && $orderId) {
            $productList = Product::limit(8)->get();
            $productListShuffle = $productList->toArray();

            shuffle($productListShuffle);

            $productRandom = $this->getRandomProduct($productList);
            $productListShuffle[4] = $productRandom->toArray();

            ProductWinner::create([
                'product_id' => $productRandom->getProductId(),
                'customer_name' => $customerName,
                'customer_phone' => $customerPhone,
                'order_id' => $orderId,
            ]);

            return $productListShuffle;
        }

        return response(null, 404);
    }

    protected function getRandomProduct($productList)
    {
        $randomProduct = $productList[rand(0, count($productList) - 1)];
        $randomChance = rand(1, 100);

//        Нёрфим шанс выпадения
//        if ($randomChance <= 20) {
//            $randomChance = rand(1, 100);
//        }

        if ($randomChance <= $randomProduct->getProductChance()) {
            return $randomProduct;
        }

        return $this->getRandomProduct($productList);
    }
}
