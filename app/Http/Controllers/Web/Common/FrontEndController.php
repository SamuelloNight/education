<?php

namespace App\Http\Controllers\Web\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function welcome()
    {
        return view('common.welcome');
    }

    public function fortuneWheel(Request $request)
    {
        return view('common.fortuneWheel');
    }
}
