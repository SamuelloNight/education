<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class ProjectSettingsController extends Controller
{
    public function artisanCommandsInitialization()
    {
        Artisan::call('key:generate');
        Artisan::call('storage:link');

        return redirect()->route('adminFortuneWheelList');
    }
}
