<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Models\RegistrationToken;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AuthenticateController extends Controller
{
    /**
     * logout method
     */
    public function signOut(): RedirectResponse
    {
        admin()->logout();

        return redirect()->route('adminAuthSignIn');
    }

    /**
     * login method
     */
    public function signIn()
    {
        return view('admin.authenticate.signIn');
    }

    /**
     * registration method
     */
    public function signUp(Request $request)
    {
        $token = $request->get('token');
        $model = RegistrationToken::whereToken($token)->first();

        if ($model && $model->getClientToken() === $token) {
            return view('admin.authenticate.signUp', [
                'clientName' => $model->getClientName(),
                'token' => $token,
            ]);
        }

        return view('admin.authenticate.denied', [
            'type' => 'signUp'
        ]);
    }
}
