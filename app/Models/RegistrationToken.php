<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static whereToken(mixed $clientToken)
 * @property mixed guard
 * @property mixed client_name
 * @property mixed token
 */
class RegistrationToken extends Model
{
    use HasFactory;

    protected $table = 'registration_tokens';

    protected $fillable = [
        'guard',
        'client_name',
        'token',
    ];

    public function getClientGuard()
    {
        return $this->guard;
    }

    public function getClientName()
    {
        return $this->client_name;
    }

    public function getClientToken()
    {
        return $this->token;
    }
}
