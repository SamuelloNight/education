<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property mixed id
 * @property mixed product_id
 * @property mixed customer_name
 * @property mixed customer_phone
 * @property mixed order_id
 * @property mixed updated_at
 * @property mixed created_at
 * @method static create(array $array)
 * @method static find(mixed $getProductWinnerProductId)
 * @method static whereStatus(string $filter)
 */
class ProductWinner extends Model
{
    use HasFactory;

    protected $table = 'product_winners';

    protected $fillable = [
        'product_id',
        'customer_name',
        'customer_phone',
        'order_id',
        'status',
    ];

    public function product(): HasOne
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function getProductWinnerId()
    {
        return $this->id;
    }

    public function getProductWinnerProductId()
    {
        return $this->product_id;
    }

    public function getProductWinnerProduct()
    {
        return Product::find($this->getProductWinnerProductId());
    }

    public function getProductWinnerCustomerName()
    {
        return $this->customer_name;
    }

    public function getProductWinnerCustomerPhone()
    {
        return $this->customer_phone;
    }

    public function getProductWinnerOrderId()
    {
        return $this->order_id;
    }

    public function getProductWinnerUpdatedAt()
    {
        return $this->updated_at;
    }

    public function getProductWinnerCreatedAt()
    {
        return $this->created_at;
    }
}
