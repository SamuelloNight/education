<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property mixed admin_id
 * @property mixed name
 * @property mixed description
 * @property mixed preview
 * @property mixed chance
 * @property mixed id
 * @property mixed created_at
 * @property mixed updated_at
 * @method static create(array $array)
 * @method static find(\Illuminate\Routing\Route|object|string|null $route)
 * @method static limit(int $int)
 */
class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'products';

    protected $fillable = [
        'admin_id',
        'name',
        'description',
        'preview',
        'chance',
    ];

    protected $hidden = [
        'admin_id',
        'chance',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    public function admin(): HasOne
    {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }

    public function getProductId()
    {
        return $this->id;
    }

    public function getProductAdminId()
    {
        return $this->admin_id;
    }

    public function getProductAdmin()
    {
        return Admin::find($this->getProductAdminId());
    }

    public function getProductName()
    {
        return $this->name;
    }

    public function getProductDescription()
    {
        return $this->description;
    }

    public function getProductPreview()
    {
        return $this->preview;
    }

    public function getProductChance()
    {
        return $this->chance;
    }

    public function getProductCreatedAt()
    {
        return $this->created_at;
    }

    public function getProductUpdatedAt()
    {
        return $this->updated_at;
    }
}
